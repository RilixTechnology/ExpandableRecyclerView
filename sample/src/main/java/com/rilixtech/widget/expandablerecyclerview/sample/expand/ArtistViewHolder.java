package com.rilixtech.widget.expandablerecyclerview.sample.expand;

import android.view.View;
import android.widget.TextView;
import com.rilixtech.widget.expandablerecyclerview.sample.R;
import com.rilixtech.widget.expandablerecyclerview.viewholders.ChildViewHolder;

public class ArtistViewHolder extends ChildViewHolder {

  private TextView childTextView;

  public ArtistViewHolder(View itemView) {
    super(itemView);
    childTextView = itemView.findViewById(R.id.list_item_artist_name);
  }

  public void setArtistName(String name) {
    childTextView.setText(name);
  }
}
