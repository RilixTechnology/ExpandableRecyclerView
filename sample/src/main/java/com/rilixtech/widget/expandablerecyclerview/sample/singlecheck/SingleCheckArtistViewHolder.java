package com.rilixtech.widget.expandablerecyclerview.sample.singlecheck;

import android.view.View;
import android.widget.Checkable;
import android.widget.CheckedTextView;
import com.rilixtech.widget.expandablecheckrecyclerview.viewholders.CheckableChildViewHolder;
import com.rilixtech.widget.expandablerecyclerview.sample.R;

public class SingleCheckArtistViewHolder extends CheckableChildViewHolder {

  private CheckedTextView childCheckedTextView;

  public SingleCheckArtistViewHolder(View itemView) {
    super(itemView);
    childCheckedTextView = itemView.findViewById(R.id.list_item_singlecheck_artist_name);
  }

  @Override
  public Checkable getCheckable() {
    return childCheckedTextView;
  }

  public void setArtistName(String artistName) {
    childCheckedTextView.setText(artistName);
  }
}
