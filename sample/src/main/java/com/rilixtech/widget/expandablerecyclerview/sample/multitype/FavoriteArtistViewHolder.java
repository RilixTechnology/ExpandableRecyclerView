package com.rilixtech.widget.expandablerecyclerview.sample.multitype;

import android.view.View;
import android.widget.TextView;
import com.rilixtech.widget.expandablerecyclerview.sample.R;
import com.rilixtech.widget.expandablerecyclerview.viewholders.ChildViewHolder;

public class FavoriteArtistViewHolder extends ChildViewHolder {

  private TextView favoriteArtistName;

  public FavoriteArtistViewHolder(View itemView) {
    super(itemView);
    favoriteArtistName = itemView.findViewById(R.id.list_item_favorite_artist_name);
  }

  public void setArtistName(String name) {
    favoriteArtistName.setText(name);
  }

}
