package com.rilixtech.widget.expandablerecyclerview.viewholders;

import android.view.View;

import com.rilixtech.widget.expandablerecyclerview.models.ExpandableGroup;

import androidx.recyclerview.widget.RecyclerView;

/**
 * ViewHolder for {@link ExpandableGroup#items}
 */
public class ChildViewHolder extends RecyclerView.ViewHolder {

  public ChildViewHolder(View itemView) {
    super(itemView);
  }
}
